<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Item;
use App\Models\Image;
use App\Http\Resources\ItemCollection;
use App\Http\Resources\ItemResource;

class ItemController extends Controller
{
   public function getCategoryItems()
   {
      $items = Item::where('category', 'Смартфон')->get();

      //return response()->json($items);
      return new ItemCollection($items);
   }

   public function getItem($id)
   {
      $item = Item::find($id);
      //$images = $item->images;
      //$comments = $item->comments;

      //return response()->json(['item' => $item, 'images' => $images, 'comments' => $comments]);
      return new ItemResource($item);
   }
}
