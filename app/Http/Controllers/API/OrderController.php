<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Item;
use App\Models\User;
use App\Http\Resources\UserResource;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->form['address']) {
            $address = $request->form['address'];
            $date = $request->form['date'];
        }
        if ($request->form['store']) {
            $address = $request->form['store'];
            $date = now();
        }
        if ($request->form['postamate']) {
            $address = $request->form['postamate'];
            $date = now();
        }

        $order = Order::create([
            'user_id' => auth()->user()->id,
            'user_name' => $request->form['name'],
            'phone' => $request->form['phone'],
            'pay' => $request->form['pay'],
            'sum' => $request->form['sum'],
            'address' => $address,
            'date' => $date,
        ]);

        foreach ($request->items as $item) {
            $order->items()->attach(Item::where('id', $item['id'])->first(), ['qty' => $item['qty']]);
        }

        return response()->json(['messege' => 'Order success'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
