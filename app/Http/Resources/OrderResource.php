<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user_name' => $this->user_name,
            'phone' => $this->phone,
            'pay' => $this->pay,
            'address' => $this->address,
            'date' => $this->date,
            'sum' => $this->sum,
            'items' => ItemResource::collection($this->items)
        ];
    }
}
