<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ImageResource;
use App\Http\Resources\CommentResource;

class ItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'id' => $this->id,
            'size' => $this->size,
            'category' => $this->category,
            'brand' => $this->brand,
            'model' => $this->model,
            'memory' => $this->memory,
            'color' => $this->color,
            'accum' => $this->accum,
            'processor' => $this->processor,
            'dimm' => $this->dimm,
            'camera' => $this->camera,
            'price' => $this->price,
            'rating' => $this->rating,
            'year' => $this->year,
            'description' => $this->description,
            'photo' => $this->photo,
            'images' => ImageResource::collection($this->images),
            'comments' => CommentResource::collection($this->comments),
            'qty' => $this->whenPivotLoaded('item_order', function () {
                return $this->pivot->qty;
            })
        ];
    }
}
