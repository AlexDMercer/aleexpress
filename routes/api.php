<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\CommentController;
use App\Http\Controllers\API\ItemController;
use App\Http\Controllers\API\OrderController;
use App\Http\Resources\UserResource;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return new UserResource($request->user());
});

Route::post('user', 'App\Http\Controllers\Api\AuthController@index');

Route::post('category', [ItemController::class, 'getCategoryItems']);
Route::get('item/{id}', [ItemController::class, 'getItem']);

Route::apiResource('comments', CommentController::class);

Route::post('orders', [OrderController::class, 'store']);
Route::post('orders/{user}', [OrderController::class, 'index']);
