export default {
   methods: {
      currency() {
         return new Intl.NumberFormat('ru-RU', {
            style: 'currency',
            currency: 'RUB',
            minimumFractionDigits: 0,
         })
      }
   }
}