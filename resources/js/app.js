require('./bootstrap')
window.Vue = require('vue').default

import Vue from 'vue'
import App from './components/App'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import formatter from './plugins/formatter.mixin'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI)
Vue.mixin(formatter)

store.dispatch('me').then(() => {
    new Vue({
        router, store, vuetify, ElementUI,
        render: h => h(App)
    }).$mount('#app')
})