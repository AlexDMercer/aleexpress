import Vue from 'vue'
import vueRouter from 'vue-router'
import { mapActions, mapGetters } from "vuex";
Vue.use(vueRouter)

export default new vueRouter({
   mode: "history",
   base: process.env.BASE_URL,
   routes: [
      {
         path: '/',
         name: 'home',
         component: () => import('./components/pages/home'),
      },
      {
         path: '/test',
         name: 'test',
         component: () => import('./components/pages/test'),
      },
      {
         path: '/login',
         name: 'login',
         meta: { layout: 'empty' },
         component: () => import('./components/auth/login'),
      },
      {
         path: '/register',
         name: 'reg',
         meta: { layout: 'empty' },
         component: () => import('./components/auth/reg'),
      },
      {
         path: '/item-:id',
         name: 'item',
         component: () => import('./components/pages/item'),
      },
      {
         path: '/itemList',
         name: 'itemList',
         component: () => import('./components/pages/itemList'),
      },
      {
         path: '/basket',
         name: 'basket',
         component: () => import('./components/pages/basket'),
      },
      {
         path: '/order',
         name: 'order',
         component: () => import('./components/pages/order'),
      },
      {
         path: '/private',
         name: 'private',
         component: () => import('./components/pages/private'),
         children: [
            {
               path: '/private/orders',
               name: 'orders',
               component: () => import('./components/inc/private/orders'),
            },
         ]
      },
   ]
})