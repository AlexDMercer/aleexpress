const auth = {
   state: {
      user: null,
      auth: false
   },
   getters: {
      user(state) {
         return state.user
      },
      auth(state) {
         return state.auth
      }
   },
   actions: {
      async login({ dispatch }, form) {
         await axios.get('/sanctum/csrf-cookie')
         await axios.post('/login', form).catch(error => console.log(error.response.data))

         return dispatch('me')
      },
      async logout({ commit }) {
         await axios.post('/logout')
         commit('setAuth', false)
         commit('setUser', null)
      },
      async register({ dispatch }, form) {
         await axios.post('/register', form).catch(error => console.log(error.response.data))

         return dispatch('me')
      },
      me({ commit }) {
         return axios.get('/api/user').then((response) => {
            commit('setAuth', true)
            commit('setUser', response.data.data)
         }).catch(() => {
            commit('setAuth', false)
            commit('setUser', null)
         })
      }
   },
   mutations: {
      setUser(state, user) {
         state.user = user
      },
      setAuth(state, auth) {
         state.auth = auth
      }
   }
}

export default auth