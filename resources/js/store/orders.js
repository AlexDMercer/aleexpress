const orders = {
   state: {
      basketItems: []
   },
   getters: {
      basketItems(state) {
         return state.basketItems
      }
   },
   actions: {
      addToBasket({ commit }, item) {
         commit('setBasketItems', item)
      },
      delItem({ commit }, index) {
         commit('delBasketItem', index)
      },
      getBasketItemsFromLS({ commit }) {
         const basketItems = JSON.parse(localStorage.getItem('basketItems'))
         if (basketItems) {
            commit('setBasketItemsFromLS', basketItems)
         }
      },
      createOrder({ commit }, order) {
         axios.post('api/orders', order).then(res => {
            //commit('setOrder', res.data)
         })
      }
   },
   mutations: {
      setBasketItems(state, item) {
         state.basketItems.push(item);
         localStorage.setItem('basketItems', JSON.stringify(state.basketItems))
      },
      delBasketItem(state, index) {
         state.basketItems.splice(index, 1);
      },
      setBasketItemsFromLS(state, basketItems) {
         state.basketItems = basketItems
      },
   }
}

export default orders