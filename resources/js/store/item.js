const item = {
   state: {
      items: [],
      item: {},
      comments: []
   },
   getters: {
      Items(state) {
         return state.items
      },
      item(state) {
         return state.item
      },
      comments(state) {
         return state.comments
      }
   },
   actions: {
      async getCategoryItems({ commit }, category) {
         await axios.post("api/category", { category }).then(res => {
            commit('setCategoryItems', res.data.data)
         })
      },
      async getItem({ commit }, id) {
         await axios.get('api/item/' + id, { id }).then(res => {
            commit('setItem', res.data.data)
         })
      },
      addComment({ commit }, comment) {
         axios.post('api/comments', comment).then(res => {
            commit('newComment', res.data.data)
         })
      }
   },
   mutations: {
      setCategoryItems(state, items) {
         state.items = items
      },
      setItem(state, item) {
         state.item = item
      },
      newComment(state, comment) {
         state.item.comments.push(comment)
      }
   }
}

export default item