import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import item from './item'
import auth from './auth'
import orders from './orders'
Vue.use(Vuex)

const store = new Vuex.Store({
   modules: {
      item: item,
      auth: auth,
      orders: orders
   }
})

export default store