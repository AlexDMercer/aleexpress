<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'size' => '6"',
            'category' => 'Смартфон',
            'brand' => 'Xiaoni',
            'model' => 'Redmi Note 8',
            'memory' => '128 Гб',
            'color' => 'белый',
            'accum' => '4000 mAh',
            'processor' => '8 ядер',
            'dimm' => '3 Гб',
            'camera' => '21 Мп',
            'price' => '20000',
            'rating' => '4.6',
            'year' => '2020',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid harum laboriosam explicabo magni dolorem. Itaque sed, possimus modi explicabo atque molestias! Non labore exercitationem quasi esse nam doloribus a sequi!',
            'photo' => 'https://c.dns-shop.ru/thumb/st1/fit/500/500/356fdb42558cc60f270886c29fa11059/6d83097e9bb33b8a6072b1f6080bd7e647b49410351edbe8aafeea24f7b79cc9.jpg'
        ];
    }
}
