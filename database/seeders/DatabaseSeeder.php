<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Item;
use App\Models\Image;
use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Sequence;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create();
        Item::factory(10)->create()->each(function ($item) {
            $item->images()->saveMany(Image::factory(6)->state(new Sequence(
                ['path' => "https://c.dns-shop.ru/thumb/st1/fit/500/500/356fdb42558cc60f270886c29fa11059/6d83097e9bb33b8a6072b1f6080bd7e647b49410351edbe8aafeea24f7b79cc9.jpg"],
                ['path' => "https://c.dns-shop.ru/thumb/st4/fit/500/500/9aaea8ff2dc38cbe7bbf38996ef3e384/7a1629ec9c1c9ab5a6f5d5600e3d7bff70f9fc64586056dd32453142ba7a75f5.jpg"],
                ['path' => "https://c.dns-shop.ru/thumb/st4/fit/wm/2000/2000/fe62cf65325c1e682f7ccb8c47b0ed45/968277499e86f48318fbe930213ffb057aafd8c635e79ef4214681f49a998883.jpg"],
                ['path' => "https://c.dns-shop.ru/thumb/st4/fit/wm/2000/2000/a64c5716a610df045f4de757c498a8bb/4e6d85b2c59c339eedb48131d2abf6146a23dd89d029620c6e7aad9f3ef8c97c.jpg"],
                ['path' => "https://c.dns-shop.ru/thumb/st1/fit/wm/2000/2000/a38b591339df1f54cd4a9481c21d7d57/201323e5c3983f65c3ac12f3ea3a9c35e605dcb4bf7fbcc96468959788843c2d.jpg"],
                ['path' => "https://c.dns-shop.ru/thumb/st4/fit/wm/2000/2000/a1206d77cca254b8be3765594b918c54/c471fc522d2c8d50a51fb58f2b394bd3343f574b0d94522b6ec3cd08dccb1838.jpg"]
            ))->make());
            $item->comments()->saveMany(Comment::factory(1)->make());
        });
    }
}
