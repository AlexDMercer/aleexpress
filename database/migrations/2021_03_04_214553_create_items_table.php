<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('size');
            $table->string('category');
            $table->string('brand');
            $table->string('model');
            $table->string('memory');
            $table->string('color');
            $table->string('accum');
            $table->string('processor');
            $table->string('dimm');
            $table->string('camera');
            $table->integer('price');
            $table->float('rating');
            $table->integer('year');
            $table->string('photo');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
